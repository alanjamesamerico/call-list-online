package br.com.calllistonline.core.excepion;

public class IntegrantNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = -1499566680962083403L;

	public IntegrantNotFoundException(Long id) {
		super("Integrande não encontrado com o id: " + id);
	}
	
	public IntegrantNotFoundException(String name) {
		super("Integrande com o nome" + name + " não encontrado!");
	}
}
