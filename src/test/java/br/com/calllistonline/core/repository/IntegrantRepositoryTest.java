package br.com.calllistonline.core.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.calllistonline.core.model.Integrant;


@DataJpaTest
@RunWith(SpringRunner.class)
public class IntegrantRepositoryTest {
	
	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private IntegrantRepository repository;
	
	
	@Test
	public void findAllIntegrantsTest() {
		this.saveSomeIntegrants();
		List<Integrant> integrants = (List<Integrant>) this.repository.findAll();
		assertTrue(integrants.size() > 0);
	}
	
	@Test
	public void findIntegrantsByNameTest() {
		this.saveOneIntegrant();
		List<Integrant> integrants =  this.repository.findByName("Beltrano").get();
		assertEquals(1, integrants.size());
		assertThat(integrants).extracting(Integrant::getName).containsOnly("Beltrano");
	}
	
	
	
	private void saveOneIntegrant() {
		
		entityManager.persist(
				Integrant.builder()
							.name("Beltrano")
							.age(39)
							.birthday(LocalDateTime.of(1976, 02, 19, 9, 9))
							.build());
	}
	
	private void saveSomeIntegrants() {
		
		entityManager.persist(
				Integrant.builder()
							.name("Beltrano")
							.age(39)
							.birthday(LocalDateTime.of(1976, 02, 19, 9, 9))
							.build());
		
		entityManager.persist(
				Integrant.builder()
							.name("Siclano")
							.age(1)
							.birthday(LocalDateTime.of(2018, 8, 15, 9, 9))
							.build());
		
		entityManager.persist(
				Integrant.builder()
							.name("Fulano")
							.age(18)
							.birthday(LocalDateTime.of(2001, 12, 02, 9, 9))
							.build());
		
		entityManager.persist(
				Integrant.builder()
							.name("Peltrano")
							.age(49)
							.birthday(LocalDateTime.of(1969, 02, 19, 9, 9))
							.build());
		
		entityManager.persist(
				Integrant.builder()
							.name("Lubriano")
							.age(69)
							.birthday(LocalDateTime.of(1949, 03, 29, 9, 9))
							.build());
	}
}
